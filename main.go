package main

import (
	"fmt"

	"gitlab.com/reinerh/dhbw/tel21a/gameboard"
)

func main() {
	runTictactoe()
}

// Haupt-Spielfunktion. Führt das Spiel aus.
func runTictactoe() {
	fmt.Println("Willkommen bei TicTacToe")

	// Spielfeld erzeugen
	board := gameboard.NewBoard(3, 3, ' ')

	gameLoop(board)

}

// Erwartet ein Spielfeld und gibt es aus.
// TODO: Schönere Ausgabe einbauen.
func printBoard(b []string) {
	for _, row := range b {
		fmt.Println(row)
	}
}

// Führt die Hauptschleife des Spielers aus.
// Erwartet ein Spielfeld, mit dem gearbeitet werden soll.
func gameLoop(board []string) {
	currentPlayer := 'X'

	for !gameOver(board) {
		board = makeMove(board, currentPlayer)

		currentPlayer = nextPlayer(currentPlayer)
	}

	fmt.Print("Das Spiel ist beendet. Ergebnis: ")
	if isDraw(board) {
		fmt.Println("Unentschieden!")
	} else {
		fmt.Printf("Spieler %c gewinnt!\n", nextPlayer(currentPlayer))
	}
}

// Erwartet ein Spielfeld und liefert true, falls das Spiel beendet ist.
func gameOver(board []string) bool {
	return isDraw(board) || playerWins(board, 'X') || playerWins(board, 'O')
}

// Erwartet einen (eingegebenen) String und liefert true, falls dieser
// eine Ziffer zwischen 1 und 9 ist.
func inputIsValid(input string) bool {
	// Die Eingabe ist ungültig, falls sie nicht genau ein Zeichen ist oder falls sie ein Zeichen, aber keine Ziffer zwischen 1 und 9 ist.
	return !(len(input) == 0 || len(input) > 1 || input[0] < '1' || input[0] > '9')
}

// Erwartet ein Spielfeld und den Namen des aktuellen Spielers.
// Fragt den Spieler nach einem Zug und trägt ihn im Spielfeld ein.
// Liefert das veränderte Spielfeld.
func makeMove(board []string, player rune) []string {
	fmt.Println("Aktuelles Spielfeld:")
	printBoard(board)
	fmt.Printf("Spieler %c, du bist am Zug.\n", player)
	fmt.Print("Wähle eine Position (1-9): ")
	var input string
	fmt.Scanln(&input) // Eingabe einlesen.

	if !inputIsValid(input) {
		fmt.Println("Die Eingabe war nicht erlaubt. Bitte noch einmal probieren.")
		fmt.Println()
		return makeMove(board, player)
	}

	inputAsInt := int(input[0] - '1')

	row := inputAsInt / 3
	col := inputAsInt % 3
	if board[row][col] == ' ' {
		board = gameboard.Put(board, row, col, player)
		return board
	}

	fmt.Println("Die Eingabe war nicht erlaubt. Bitte noch einmal probieren.")
	fmt.Println()
	return makeMove(board, player)
}

// Erwartet den aktuellen Spieler und die Namen beider Spieler.
// Liefert den Spieler, der als nächstes am Zug ist.
func nextPlayer(currentPlayer rune) rune {
	if currentPlayer == 'X' {
		return 'O'
	}
	return 'X'
}

// Erwartet ein Spielfeld und liefert true, falls das Spiel unentschieden ist.
func isDraw(board []string) bool {
	return gameboard.NoneContains(board, ' ') && !playerWins(board, 'X') && !playerWins(board, 'O')
}

// Erwartet eine Spielfeld und einen Spielernamen.
// Liefert true, falls dieser Spieler gewonnen hat.
func playerWins(board []string, player rune) bool {
	return gameboard.AnyRowContainsOnly(board, player) || gameboard.AnyColumnContainsOnly(board, player) || gameboard.AnyDiagContainsOnly(board, player)
}
